﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valve.Observable
{
    internal interface ISubjet
    {
        void AjouteObservateur(IObservable observateur);

        void RetireObservateur(IObservable observateur);

        void NotifierObservateurs();
    }
}
