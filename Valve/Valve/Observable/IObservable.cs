﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valve.Capteur;

namespace Valve.Observable
{
    internal interface IObservable
    {
        void Actualiser(ICapteur sujet);
    }
}
