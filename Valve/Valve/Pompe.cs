﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibIO;
using Valve.Capteur;
using Valve.Observable;
using Valve.State;

namespace Valve
{
    internal class Pompe: IObservable
    {
        public Dictionary<string, EtatAbstract> mappingEtat { get; set; } = new Dictionary<string, EtatAbstract> {
            {"fermer", new EtatFermePompe() },
            {"ouvert", new EtatOuvertPompe() },
            {"defaut", new EtatDefautPompe() },
        };

        public EtatAbstract etat { get; set; }
        public CapteurTOR capteurTOREtat { get; set; }
        public CapteurTOR capteurTORDef { get; set;  }
        public CapteurTOR capteurTORMA { get; set;  }

        public int id {  get; set; }

        public Pompe(int idPompe)
        {
            etat = mappingEtat["fermer"];
            id = idPompe;

            if (idPompe == 1)
            {
                capteurTOREtat = new CapteurTOR(MapIO.PMP0_ETAT);
                capteurTORDef = new CapteurTOR(MapIO.PMP0_DEF);
                capteurTORMA = new CapteurTOR(MapIO.PMP0_MA);
            } else if (idPompe == 2)
            {
                capteurTOREtat = new CapteurTOR(MapIO.PMP1_ETAT);
                capteurTORDef = new CapteurTOR(MapIO.PMP1_DEF);
                capteurTORMA = new CapteurTOR(MapIO.PMP1_MA);
            } else if (idPompe == 3)
            {
                capteurTOREtat = new CapteurTOR(MapIO.PMP2_ETAT);
                capteurTORDef = new CapteurTOR(MapIO.PMP2_DEF);
                capteurTORMA = new CapteurTOR(MapIO.PMP2_MA);
            }

            capteurTOREtat.AjouteObservateur(this);
            capteurTORDef.AjouteObservateur(this);
            capteurTORMA.AjouteObservateur(this);
        }

        public void Actualiser(ICapteur sujet)
        {
            if (sujet == capteurTORDef)
            {
                Console.WriteLine($"Actualisation de la pompe {id} capteur DEF: {capteurTORDef.getVal()}");

                if (capteurTORDef.getVal())
                {
                    etat = mappingEtat["defaut"];
                }
            } else if (sujet == capteurTOREtat)
            {
                Console.WriteLine($"Actualisation de la pompe {id} capteur ETAT: {capteurTOREtat.getVal()}");
            } else if (sujet == capteurTORMA)
            {
                Console.WriteLine($"Actualisation de la pompe {id} capteur MA: {capteurTORMA.getVal()}");
                etat = mappingEtat[(capteurTORMA.getVal() ? "ouvert" : "fermer")];
            }
        }

        public void OuvrirPompe()
        {
            etat = new EtatOuvertPompe();
        }
        public void FermerVanne()
        {
            etat = new EtatFermePompe();

        }
        public void DefautVanne()
        {
            etat = new EtatDefautPompe();
        }
    }
}
