﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LibIO;

namespace Valve
{
    internal class IO
    {
        private CLibIO io;

        private static IO _instance;

        private IO()
        {
            //  Les information sur le serveur distant d'acquisition
            string nomServeur = "127.0.0.1";
            int portServeur = 1069;
            int portClient = 0;

            //  Le booléen pour les sorties ToR
            bool sTor = false;

            uint compteur = 0;

            try
            {
                //  instanciation des IO
                Console.Write("Instanciation LibIO({0},{1},{2})", nomServeur, portServeur, portClient);
                io = new CLibIO(nomServeur, portServeur, portClient);

                // Il faut laisser un peu de temps à la communication pour s'établir...
                Thread.Sleep(1000);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Erreur... Pas de serveur ?");
                Console.WriteLine(ex.Message);
                Console.WriteLine("Syntaxe: ConsoleTestSim IPServeur(def: 127.0.0.1) PortServeur(def: 1069) POrtClient(def: 0)");
                System.Environment.Exit(1);
            }
        }

        public static IO GetInstance()
        {
            if (_instance == null)
            {
                lock (typeof(IO))
                {
                    if (_instance == null)
                    {
                        _instance = new IO();
                    }
                }
            }
            return _instance;
        }

        public bool get_entreeToR_Nb(uint i)
        {
            return io.get_entreeToR_Nb(i);
        }

        public void set_sortieToR_Nb(uint i, bool val)
        {
            io.set_sortieToR_Nb(i, val);
        }

        public double get_entreeAn_Nb(uint i)
        {
            return io.get_entreeAn_Nb(i);
        }
    }
}
