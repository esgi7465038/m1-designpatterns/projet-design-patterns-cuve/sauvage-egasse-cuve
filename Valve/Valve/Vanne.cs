﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibIO;
using Valve.Capteur;
using Valve.Observable;
using Valve.State;

namespace Valve
{
    internal class Vanne: IObservable
    {
        public Dictionary<string, EtatAbstract> mappingEtat {  get; set; } = new Dictionary<string, EtatAbstract> {
            {"fermer", new EtatFermeVanne() },
            {"ouvert", new EtatOuvertVanne() },
            {"defaut", new EtatDefautVanne() },
        };

        public EtatAbstract etat = new EtatDefautVanne();
        public CapteurTOR capteurTOROuvert { get; set; }
        public CapteurTOR capteurTORFermer { get; set; }
        public CapteurAN capteurANDebit { get; set; }

        public double debit {  get; set; }

        public Vanne() 
        {
            capteurTOROuvert = new CapteurTOR(MapIO.C_VAN_OUV);
            capteurTORFermer = new CapteurTOR(MapIO.C_VAN_FERM);
            capteurANDebit = new CapteurAN(MapIO.DEBIT);


            capteurTOROuvert.AjouteObservateur(this);
            capteurTORFermer.AjouteObservateur(this);
            capteurANDebit.AjouteObservateur(this);
        }

        public void Actualiser(ICapteur sujet)
        {
            if (sujet == capteurANDebit)
            {
                debit = capteurANDebit.getVal();
                Console.WriteLine($"Actualisation du débit de la vanne: {debit}");
            }
            else if (sujet == capteurTORFermer)
            {
                etat = mappingEtat["fermer"];
            }
            else if (sujet == capteurTOROuvert)
            {
                etat = mappingEtat["ouvert"];
            }
    
            Console.WriteLine($"1 Actualisation de la vanne: {etat.getStr()}");
        }

    }
}
