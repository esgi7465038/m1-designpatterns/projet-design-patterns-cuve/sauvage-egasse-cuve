﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibIO;
using Valve.Capteur;
using Valve.Observable;
using Valve.State;

namespace Valve
{
    internal class Cuve: IObservable
    {
        public Dictionary<string, EtatAbstract> mappingEtat { get; set; } = new Dictionary<string, EtatAbstract> {
            {"vide", new EtatVideCuve() },
            {"bas", new EtatBasCuve() },
            {"haut", new EtatHautCuve() },
            {"plein", new EtatPleinCuve() },
        };

        public EtatAbstract etat { get; set; }
        public CapteurTOR capteurHaut { get; set; }
        public CapteurTOR capteurMilieuHaut { get; set; }
        public CapteurTOR capteurMilieuBas { get; set; }
        public CapteurTOR capteurBas { get; set; }

        public Cuve()
        {
            etat = mappingEtat["vide"];
            capteurHaut = new CapteurTOR(MapIO.CUVE_PLEINE);
            capteurMilieuHaut = new CapteurTOR(MapIO.CUVE_HAUT);
            capteurMilieuBas = new CapteurTOR(MapIO.CUVE_BAS);
            capteurBas = new CapteurTOR(MapIO.CUVE_VIDE);

            capteurBas.AjouteObservateur(this);
            capteurHaut.AjouteObservateur(this);
            capteurMilieuHaut.AjouteObservateur(this);
            capteurMilieuBas.AjouteObservateur(this);
        }

        public void Actualiser(ICapteur sujet)
        {
            if (sujet == capteurBas)
            {
                if (!capteurMilieuBas.getVal() && !capteurBas.getVal()) etat = mappingEtat["vide"];
                Console.WriteLine($"actualiser cuve capteur vide: {capteurBas.getVal()}");
            }
            else if (sujet == capteurMilieuBas)
            {
                if (capteurBas.getVal() && !capteurMilieuHaut.getVal() && capteurMilieuBas.getVal()) etat = mappingEtat["bas"];
                Console.WriteLine($"actualiser cuve capteur bas: {capteurMilieuBas.getVal()}");
            }
            else if (sujet == capteurMilieuHaut)
            {
                if (capteurMilieuBas.getVal() && !capteurHaut.getVal() && capteurMilieuHaut.getVal()) etat = mappingEtat["haut"];
                Console.WriteLine($"actualiser cuve haut: {capteurMilieuHaut.getVal()}");
            }
            else if (sujet == capteurHaut)
            {
                if (capteurMilieuHaut.getVal() && capteurHaut.getVal()) etat = mappingEtat["plein"];

                Console.WriteLine($"actualiser cuve plein: {capteurHaut.getVal()}");
            }
        }
    }
}
