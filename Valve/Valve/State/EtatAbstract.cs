﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valve.State
{
    internal class EtatAbstract
    {
        private string etatStr = "defaut";

        public EtatAbstract(string name) {
            etatStr = name;
        }
        public string getStr()
        {
            return etatStr;
        }
    }
}
