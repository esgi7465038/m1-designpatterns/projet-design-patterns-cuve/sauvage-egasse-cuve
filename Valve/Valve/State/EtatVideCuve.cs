﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valve.State
{
    internal class EtatVideCuve: EtatAbstract
    {
        public EtatVideCuve() : base("vide")
        {
        }
    }
}
