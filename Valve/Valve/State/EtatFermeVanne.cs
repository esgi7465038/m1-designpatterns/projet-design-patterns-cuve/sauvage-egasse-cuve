﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valve.State
{
    internal class EtatFermeVanne: EtatAbstract
    {
        public EtatFermeVanne() : base("fermer")
        {
        }
    }
}
