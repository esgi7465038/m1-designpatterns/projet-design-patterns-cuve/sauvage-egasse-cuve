﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LibIO;

namespace Valve
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IO.GetInstance();
            Vanne vanne = new Vanne();
            List<Pompe> pompes = new List<Pompe> { new Pompe(1), new Pompe(2), new Pompe(3) };
            Cuve cuve = new Cuve();   

            bool isRunning = true;
            string exitStr = "3";

            Thread.Sleep(100);
            Console.Clear();

            while (isRunning)
            {
                Console.WriteLine("0 - Voir le status de la vanne");
                Console.WriteLine("1 - Voir le status des pompes");
                Console.WriteLine("2 - Voir le status de la cuve");
                Console.WriteLine($"{exitStr} - exit");

                string input = Console.ReadLine();
                if (input.Equals(exitStr, StringComparison.OrdinalIgnoreCase))
                {
                    isRunning = false;
                } else
                {
                    Console.Clear();

                    switch (input.ToLower())
                    {
                        case "0":
                            Console.WriteLine("Etat de la vanne: ");
                            Console.WriteLine($"Debit: {vanne.debit}");
                            Console.WriteLine($"Etat: {vanne.etat.getStr()}");
                            Console.WriteLine("\n");
                            break;
                        case "1":
                            for (int i = 0; i < MapIO.NB_POMPE; i++)
                            {
                                Console.WriteLine($"Etat de la pompe {i+1}: {pompes[i].etat.getStr()}");
                            }
                            break;
                        case "2":
                            Console.WriteLine($"Etat de la cuve: \n\t Etat: {cuve.etat.getStr()} \n\t Capteur Plein: {cuve.capteurHaut.getVal()} \n\t Capteur Haut: {cuve.capteurMilieuHaut.getVal()} \n\t Capteur bas: {cuve.capteurMilieuBas.getVal()} \n\t Capteur vide: {cuve.capteurBas.getVal()}");
                            break;
                    }   

                }

                Console.WriteLine("Appuyer sur une touche pour continuer");
                Console.ReadLine();
                Console.Clear();
            }
        }
    }
}
