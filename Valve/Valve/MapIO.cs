﻿namespace LibIO
{
    public sealed class MapIO
    {
        /****************************************************************
        *Nombre de pompes pour alimenter la cuve.
        *****************************************************************/
        public const int NB_POMPE = 3;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR cuve vide.
        *****************************************************************/
        public const int CUVE_VIDE = 0;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR niveau cuve basse.
        *****************************************************************/
        public const int CUVE_BAS = 1;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR niveau cuve haut.
        *****************************************************************/
        public const int CUVE_HAUT = 2;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR cuve pleine.
        *****************************************************************/
        public const int CUVE_PLEINE = 3;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR d'état de la pompe N° 0.
        *****************************************************************/
        public const int PMP0_ETAT = 4;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR d'état de la pompe N° 1.
        *****************************************************************/
        public const int PMP1_ETAT = 5;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR d'état de la pompe N° 2.
        *****************************************************************/
        public const int PMP2_ETAT = 6;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR de défaut de la pompe N° 0.
        *****************************************************************/
        public const int PMP0_DEF = 7;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR de défaut de la pompe N° 1.
        *****************************************************************/
        public const int PMP1_DEF = 8;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR de défaut de la pompe N° 2.
        *****************************************************************/
        public const int PMP2_DEF = 9;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR vanne fermée.
        *****************************************************************/
        public const int C_VAN_FERM = 10;

        /****************************************************************
        *N° de l'entrée pour le capteur ToR vanne ouverte.
        *****************************************************************/
        public const int C_VAN_OUV = 11;

        /****************************************************************
        *Adresse de base des sorties ToR sur le système d'acquisition.
        *****************************************************************/
        public const int BASE_STOR = 0xA010;

        /****************************************************************
        *N° de la sortie ToR pour Marche/Arrêt de la pompe N° 0.
        *****************************************************************/
        public const int PMP0_MA = 0;

        /****************************************************************
        *N° de la sortie ToR pour Marche/Arrêt de la pompe N° 1.
        *****************************************************************/
        public const int PMP1_MA = 1;

        /****************************************************************
        *N° de la sortie ToR pour Marche/Arrêt de la pompe N° 2.
        *****************************************************************/
        public const int PMP2_MA = 2;

        /****************************************************************
        *N° de la sortie ToR pour Ouverture/Fermeture de la vanne.
        *****************************************************************/
        public const int VAN_OF = 3;

        /****************************************************************
        *Adresse de base des entrées analogiques sur le système d'acquisition.
        *****************************************************************/
        public const int BASE_EAN = 0xA020;

        /****************************************************************
        *N° de l'entrée An. pour le capteur de hauteur dans la cuve.
        *****************************************************************/
        public const int HAUTEUR = 0;

        /****************************************************************
        *N° de l'entrée An. pour le capteur de débit en sortie de vanne.
        *****************************************************************/
        public const int DEBIT = 1;
    }
}
