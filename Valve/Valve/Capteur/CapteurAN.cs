﻿using System;
using System.Collections.Generic;
using System.Threading;
using Valve.Capteur;
using Valve.Observable;

namespace Valve
{
    internal class CapteurAN: ICapteur
    {

        public double ValObservee { get; private set; } = 0;
        private readonly List<IObservable> Observateurs = new List<IObservable>();
        public readonly Thread thread;
        public int id = -1;
        public bool isRunning = true;


        public CapteurAN(int idGive)
        {
            id = idGive;
            thread = new Thread(Simulation);
            thread.Start();
        }   

        public void AjouteObservateur(IObservable observateur)
        {
            Observateurs.Add(observateur);
        }

        public void RetireObservateur(IObservable observateur)
        {
            Observateurs.Remove(observateur);
        }
        

        public void NotifierObservateurs()
        {
            foreach (IObservable observateur in Observateurs)
            {
                observateur.Actualiser(this);
            }
        }

        private void Simulation()
        {
            while (isRunning)
            {
                var newValeur = IO.GetInstance().get_entreeAn_Nb((uint)id);

                if (newValeur != ValObservee)
                {
                    Console.WriteLine(newValeur);
                    ValObservee = newValeur;
                    NotifierObservateurs();
                }

                Thread.Sleep(1000);
            }
        }

        public int getId()
        {
            return id;
        }

        public double getVal()
        {
            return ValObservee;
        }

    }
}
