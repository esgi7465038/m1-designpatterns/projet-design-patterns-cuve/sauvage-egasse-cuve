﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valve.Observable;

namespace Valve.Capteur
{
    internal interface ICapteur: ISubjet
    {
        int getId();
    }
}
