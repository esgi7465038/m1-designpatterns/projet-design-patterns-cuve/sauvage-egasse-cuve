﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Valve.Capteur;
using Valve.Observable;

namespace Valve
{
    internal class CapteurTOR: ICapteur
    {
        public bool ValObservee { get; private set; }
        private readonly List<IObservable> Observateurs = new List<IObservable>();
        public readonly Thread thread;
        public bool isRunning = true;
        public int id = -1;

        public CapteurTOR()
        {
            thread = new Thread(Simulation);
            thread.Start();
            id = 1;
        }

        public CapteurTOR(int idGive)
        {
            id = idGive;
            thread = new Thread(Simulation);
            thread.Start();
        }

        public void AjouteObservateur(IObservable observateur)
        {
            Observateurs.Add(observateur);
        }

        public void RetireObservateur(IObservable observateur)
        {
            Observateurs.Remove(observateur);
        }


        public void NotifierObservateurs()
        {
            foreach (IObservable observateur in Observateurs)
            {
                observateur.Actualiser(this);
            }
        }

        private void Simulation()
        {
            while (isRunning)
            {
                var newValeur = IO.GetInstance().get_entreeToR_Nb((uint)id);
                   
                if (newValeur != ValObservee)
                {
                    ValObservee = newValeur;
                    NotifierObservateurs();
                }

                Thread.Sleep(1000);
            }
        }

        public int getId()
        {
            return id;
        }

        public bool getVal()
        {
            return ValObservee;
        }
    }
}
