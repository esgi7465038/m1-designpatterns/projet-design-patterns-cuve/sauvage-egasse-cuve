# Projet cuve

*Timothée SAUVAGE, Erwan EGASSE*

---

## Préface

Au coeur de notre projet cuve, nous avons utilisé les designs patterns *observers* et *state*.

## Designs utilisés

Les observers sont au coeur de nos classes héritant de `ICapteur`.
Quant au state, il est utilisé autant que possible, là où sa présence est utile; par exemple : La cuve, les vannes, ainsi que les pompes.

## Code

Nous disposons, dans le dossier "capteurs", de trois fichiers. L'un correspond à l'interface "ICapteur", qui est implémentée dans les classes voisines "CapteurAN" & "CapteurTOR".

Dans les "Observables" se trouvent se trouvent deux interfaces. Le premier "IObservable" et le second "ISubject".

Les "States" contiennent tous les états possibles pour nos différentes classes métier.

Nous avons `EtatAbstract` qui est implémenté sous les différentes classes suivantes :

- `EtatVideCuve`
- `EtatBasCuve`
- `EtatHautCuve`
- `EtatPleinCuve`

- `EtatDefautPompe`
- `EtatFermePompe`
- `EtatOuvertPompe`

- `EtatDefautVanne`
- `EtatFermeVanne`
- `EtatOuvertVanne`

## Diagramme UML

![](./uml.png)
